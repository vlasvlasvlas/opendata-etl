import calendar
import csv
import datetime
import json
import logging
import os
from urllib.parse import urlparse

import flattentool
import psycopg2
from yaml import Loader, load

import config
from const import DATASET_CONF_FILE, EXTENDED_SCHEMA_FILE, DATA_BASE_PATH, BASE_JSON_FILE, PACKAGE_DATA, OCDS_DATASET

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def get_connection():
    """ Método que toma la configuración del archivo config.py para
    crear una conexión a la base de datos

    Returns:
        connection: La conexión a la base de datos
    """
    try:
        result = urlparse(config.database)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        connection = psycopg2.connect(
            database=database,
            user=username,
            password=password,
            host=hostname
        )
        return connection
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)


def get_initial_end_date(year, month):
    inicio, fin = calendar.monthrange(year, month)
    month = '0' + str(month) if len(str(month)) < 2 else month
    fecha_desde = '\'{}-{}-01\''.format(year, month)
    fecha_hasta = '\'{}-{}-{}\''.format(year, month, fin)
    return fecha_desde, fecha_hasta


def format_date_to_query(date):
    return '\'{}\''.format(date)


def get_year_until():
    return 2019
#    return datetime.datetime.now().year + 1


def get_dataset_to_process():
    """
    Ths method reads the datasets_conf.yml file and return it as a dict.
    :return:
    """
    with open(DATASET_CONF_FILE, 'r') as datasets_conf:
        return load(datasets_conf, Loader=Loader)


def load_json(filename, collection, type):
    with open(filename, 'r', encoding='utf-8') as f:
        if type == 'json':
            yield json.load(f)[collection['table']]
        else:
            yield csv.DictReader(f)


def get_csvs_names():
    unique_archivos = {}
    for year in range(2018, 2019):
        for mes in range(1, 13):
            path = os.path.join(get_directory(folder=OCDS_DATASET, anio='{}-{}'.format(year, mes)),'flattened-titles')
            archivos = os.listdir(path)
            for archivo in archivos:
                if archivo not in unique_archivos:
                    unique_archivos[archivo] = {'archivos': [], 'year': year}
                unique_archivos[archivo]['archivos'].append(os.path.join(path, archivo))
    return unique_archivos


def generate_csvs():
    archivos = get_csvs_names()
    for archivo in archivos:
        path = os.path.join(get_directory(OCDS_DATASET, anio='final-csv-{}'.format(archivos[archivo]['year'])), archivo)
        join_csvs(archivos[archivo]['archivos'], path)


def join_csvs(csv_list, output_name):
    import csv
    # First determine the field names from the top line of each input file
    # Comment 1 below
    fieldnames = []
    for filename in csv_list:
        with open(filename, "r", newline="") as f_in:
            reader = csv.reader(f_in)
            headers = next(reader)
            for h in headers:
                if h not in fieldnames:
                    fieldnames.append(h)

    # Then copy the data
    with open(output_name, "w", newline="") as f_out:  # Comment 2 below
        writer = csv.DictWriter(f_out, fieldnames=fieldnames)
        writer.writeheader()
        for filename in csv_list:
            with open(filename, "r", newline="") as f_in:
                reader = csv.DictReader(f_in)  # Uses the field names in this file
                for line in reader:
                    # Comment 3 below
                    writer.writerow(line)


def convert_to_json(path, name, list_name, ocds=True):
    logging.info('Convirtiendo a json %s ' % name)
    encoding = 'utf-8-sig'
    flatten_tool_options = {
        'output_name': os.path.join(path, name),
        'input_format': 'csv',
        'base_json': get_base_metadata() if ocds else None,
        'schema': EXTENDED_SCHEMA_FILE if ocds else None,
        'default_configuration': 'RootListPath {}'.format(list_name),
        'encoding': encoding,
        'disable_local_refs': True,
    }
    flattentool.unflatten(
        path,
        **flatten_tool_options
    )


def get_base_metadata():
    with open(BASE_JSON_FILE, 'w') as outfile:
        json.dump(PACKAGE_DATA, outfile)
        return BASE_JSON_FILE


def get_directory(folder=None, name=None, anio=None):
    if folder and anio:
        directory = "{}/{}/{}".format(DATA_BASE_PATH, folder, anio)
    elif folder and not anio:
        directory = "{}/{}".format(DATA_BASE_PATH, folder)
    else:
        directory = "{}/no-ocds/{}".format(DATA_BASE_PATH, name)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


def wait_process(processes):
    for process in processes:
        process.join()

