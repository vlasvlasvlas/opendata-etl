import os


class Config:
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_precious_secret_key')
    DEBUG = False
    DATABASE_URL = os.environ['DATABASE_URL']
    ELASTIC_SEARCH_URL = os.environ['ELASTIC_URL']
    DATA_PATH = os.environ['DATA_PATH']


elastic = Config.ELASTIC_SEARCH_URL
database = Config.DATABASE_URL
data_path = Config.DATA_PATH
