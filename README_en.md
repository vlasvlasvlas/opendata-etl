# [Kavure'i](https://gn.wikipedia.org/wiki/Kavure%27i): ETL for an Open data portal

![Image of Kavurei](https://upload.wikimedia.org/wikipedia/commons/5/59/Cactus_Ferruginous_Pygmy-owl.jpg)

This project is ETL that **e**xtracts data from a postgresql database
using SQL queries in flattenized/releases format, according to [https://flatten-tool.readthedocs.io/en/latest/examples/](https://flatten-tool.readthedocs.io/en/latest/examples/),
save them as CSV using [pandas](https://pandas.pydata.org/), **t**ransform them to JSON using a [flatten-tool](https://flatten-tool.readthedocs.io/),
**l**oad them as individual OCDS JSON releases in a [Elastic Search database](https://www.elastic.co/en/), generates the OCDS records given all the inserted releases to ElasticSearch and finally generates a final csv with the bulk compiled releases per year. 

The Elastic Search database normally will be used to provide an API to access the data for tools. And the bulk csv file to users that wants to analyse the historical data.

## Query format

An example of how queries should be defined can be found in the queries folder.
Considerations for OCDS queries:
- They must have the flattenized format of the standard, defined in [https://flatten-tool.readthedocs.io/en/latest/examples/](https://flatten-tool.readthedocs.io/en/latest/examples/ )
- In order for a query to run, it must be listed in the datasets_conf.yml file. If it is a query for an OCDS release then it should go under the section of *ocds* if it is a different dataset should go in the section of *no_ocds*
- The filter parameters to pass to the query must be specified in the format {0} where 0 is the order of the parameters to pass.
The script is prepared to receive 2 parameters for OCDS, a start date and an end date to search the data.
- Each query will generate a CSV file, that will later be transformed to 1 JSON file of OCDS releases
- Each query and therefore each resulting CSV file is made up of OCDS objects or arrays. For example, there is a query to get all the tenders, which will finally be the csv tender.csv and then for each array within tender, for example documents, a new csv must be added, indicating in this
new document query the id of the tender to which the document corresponds (see in the example the tender/id field inside the file tender-documents.sql)
This will make the JSON conversion tool join the tender object with the array of all documents and the same for each of the arrays that has the tender object and the other objects of the OCDS standard.
- In order to join all the data together, for example, tender with their tender/documents the queries specified in both files must also share the same **id** of the release, with this id the flatten tool is able to gather the information in the same object, in this case tender.
- BEWARE!: It is recommended that queries, and therefore releases, be separated by a time range, as shown in the example query. Depending on the volume of data this range can be one month, one year, etc. This is due the performance of the flatten tool to perform the conversion from csv to json without usign all the memory of the computer where the script is running.

## datasets_conf file format

This file lists all the queries to execute and groups them according to whether they are ocds releases or another type of data set.
If the query to add is part of a OCDS release then it must go under the *ocds* list, if not, under the *no-ocds* list.
For ocds queries, the following fields must be specified:
- name: the name of the object to be generated, this name is then used as the name of the csv to be generated
- query_path: the path to the query to execute. Optionally, since in postgresql the names of the columns can only have up to 63 characters and some columns could have more than this, you can specify in the queries the name of the column with a prefix, which will then be replaced by its correct name when generating the CSV, for example:
    - key: line
    - prefix: planning/budget/budgetBreakdown/0
So in your queries, you can use the "line" word instead of "planning/budget /budgetBreakdown/0" and the script will do the replacement for you.
   
For *no-ocds* queries, the following fields must be specified, for example, if you have the suppliers dataset:
  - name: name of the index in elasticsearch, example: suppliers
  - table: name of the csv and index to create in the elasticsearch, example: suppliers
  - id: name of the field to use as id, it must be unique in the dataset, example id
  - type: type of object in the elasticsearch to create, example supplier
  - datasets: list of queries belonging to this datasets, they can be several if the object has arrays as property, and for each dataset:
      - id: id
      - name: suppliers
      - query_path: queries/proveedores/proveedores.sql

## Environment variables and data to configure

In order to run the scripts 3 environment variables must be configured:

- DATABASE_URL: the url to the database from which the data will be extracted
- ELASTIC_URL: the url to the elastic search database in which the data will be loaded
- DATA_PATH: the path in which they will be saved

Also, in the file const.py in the variable PACKAGE_DATA, the meta data of the OCDS publication must be configured,
such as:
- The publisher
- The URL to the publication policy
- The URL to the license
- The version of the standard used
- The list of extensions used

In addition, the INITIAL_YEAR variable must be configured to determine from which year the data should be searched for conversion.

## Execution

### With virtual env
- Create a virtual environment by executing: `virtualenv -p python3 .ve`
- Activate the virtual environment with `source .ve/bin/activate`
- Install project dependencies with `pip install -r requirements.txt`
- Set the environment variables DATABASE_URL, DATA_PATH and ELASTIC_URL for example:
    `export DATABASE_URL = test`
    `export ELASTIC_URL = http://172.17.0.1:9200`
    `export DATA_PATH = /home/user/data`
- There are two availables scripts to execute:

1) convert_historical: This script is prepared to do a massive search per month from the year set in INITIAL_YEAR to the year current. To run it you have to run `python covert_historical.py`. This command should be run only once to extract, transform and load all existing data. It has the following options:
- --delete-indexes: Clear OCDS indexes from elasticsearch before starting
- --only-ocds: Only perform ETL for ocds data sets
- --only-no-ocds: Only performs ETL for non-ocds data sets
- --only-transform: Only the transformation step is performed (conversion to JSON of existing CSVs)
- --only-extract: Only the extraction step is performed (execution of queries)
- --only-load: Only load step is performed (loading existing JSON to ElasticSearch)
- -d: Instead of excecute al the queries, indicates the list of datasets to execute
- --only-final-csv: It only generates the final csv (this is the bulk csv)

2) convert: this script is prepared to execute queries taking into account the script's last execution date, that is, the range of dates to search goes from the last date of execution to the current date. This script is the one that must be configured, for example, in a CRON
to update the data according to a certain frequency, for example, every day, every hours,etc. It runs with `python covert.py` and has the following options:
- --only-ocds: Only perform ETL for ocds
- --only-no-ocds: Only performs ETL for non-ocds
- -d: Indicates the list of datasets to execute
- --only-final-csv: It only generates the final csv (this is the bulk csv)

The process of generating final files of 1 json/csv per year is separate because the user may want, for example, to update the bulk fiels once per day but update the ElasticSearch database more regularly, for example every 1 hour.
