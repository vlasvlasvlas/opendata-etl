import datetime

import config

RELEASES_BASE_URL = 'https://www.contrataciones.gov.py/datos/api/v2/releases/'
DATA_BASE_PATH = config.data_path
DATASET_CONF_FILE = 'datasets_conf.yml'
RELEASES = 'releases'
OCDS_VERSION = '1.1'
OCDS_DATASET = 'ocds'
NO_OCDS_DATASET = 'no-ocds'
RECORDS = 'records'
BASE_JSON_FILE = 'base.json'
EXTENDED_SCHEMA_FILE = 'extended_release_schema.json'
INITIAL_YEAR = 2018
MAX_DOCUMENTS_AT_TIME = 200
DATASETS_KEY = 'datasets'

PACKAGE_DATA = {
    "uri": "https://www.contrataciones.gov.py/datos/record-package/193399.json",
    "publishedDate": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
    "publisher": {
        "name": "Dirección Nacional de Contrataciones Públicas, Paraguay"
    },
    "license": "https://creativecommons.org/licenses/by/4.0/",
    "publicationPolicy": "https://www.contrataciones.gov.py/datos/legal",
    "version": "1.1",
    "extensions": ["https://raw.githubusercontent.com/open-contracting-extensions/ocds_budget_breakdown_extension"
                   "/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_budget_and_spend_extension"
                   "/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_lots_extension/master"
                   "/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_enquiry_extension/master"
                   "/extension.json",
                   "https://raw.githubusercontent.com/sdd1982/enquiriesAddress/master/extension.json",
                   "https://raw.githubusercontent.com/sdd1982/legalEntityTypeDetail/master/extension.json",
                   "https://raw.githubusercontent.com/gitMHOCDS/transactions/master/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_planning_investmentproject_extension/raw/master/extension"
                   ".json",
                   "https://gitlab.com/dncp-opendata/ocds_clarification_meetings_extension/raw/master/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_item_attributes_extension/raw/master/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_planning_items_extension/raw/master/extension.json",
                   "https://raw.githubusercontent.com/gitMHOCDS/dncp-contract-code/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_contract_suppliers_extension"
                   "/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_partyDetails_scale_extension"
                   "/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_location_extension/v1.1.4"
                   "/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_partyDetails_activityType_extension/raw/master/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_tender_notifiedsuppliers_extension/raw/master/extension.json",
                   "https://gitlab.com/dncp-opendata/ocds_tender_lots_details_extension/raw/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions"
                   "/ocds_memberOf_extension/master/extension.json",
                   "https://raw.githubusercontent.com/portaledcahn/ocds_tenderDatePublished_extension/master"
                   "/extension.json", "https://raw.githubusercontent.com/INAImexico/ocds_guarantees_extension/master"
                                      "/extension.json", "https://bitbucket.org/ONCAETI/ocds_releasesource_extension"
                                                         "/raw/master/extension.json", "https://raw.githubusercontent"
                                                                                       ".com/open"
                                                                                       "-contracting-extensions"
                                                                                       "/ocds_requirements_extension"
                                                                                       "/master/extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_bidOpening_extension/master/"
                   "extension.json",
                   "https://raw.githubusercontent.com/open-contracting-extensions/ocds_coveredBy_extension/master/"
                   "extension.json", "https://raw.githubusercontent.com/open-contracting-extensions"
                                     "/ocds_qualification_extension/master/extension.json", "https://raw"
                                                                                            ".githubusercontent.com"
                                                                                            "/open-contracting"
                                                                                            "-extensions"
                                                                                            "/ocds_requirements"
                                                                                            "_extension/master/"
                                                                                            "extension.json"]
}
