# -*- coding: utf-8 -*-
"""Importador de datos de la base de datos de la DNCP a formatos abiertos en Elasticsearch

Esta clase tiene como función tomar querys sobre las tablas relacionales de la base de datos de la
DNCP, ejecutarlos, transformarlos y guardarlos finalmente como JSON en Elasticsearch.

Actualmente soporta dos tipos de queries:

- ocds:
    Los queries OCDS se refieren a aquellos conjuntos de datos que requieran ser publicados en
    formato Open Contracting Data Standard. Para poder hacer esta transformación se toman todos los queries
    listados bajo ocds, se ejecutan, se guardan como CSV, se convierten a JSON con la herramienta flattening tool
    OCDS y se envian estos jsons al índice 'releases' del Elasticsearch

- No ocds:
    Para los otros conjuntos de datos de la DNCP que no van dentro del estándar OCDS se procede solamente a
    ejecutar los queries, guardarlos como CSV y enviarlos directamente al Elasticsearch

"""
import argparse
import gc
import logging
import multiprocessing
import os

from const import RELEASES, NO_OCDS_DATASET, DATASETS_KEY, RECORDS, INITIAL_YEAR
from convert_base import load_releases_ocds, extract_ocds, load, generate_csv, all_ocids
from elastic_helper import insert_records_into_elastic, \
    initial_config, delete_index
from helper import get_directory, convert_to_json, get_dataset_to_process, get_year_until, \
    get_initial_end_date, wait_process, format_date_to_query
from ocds_helper import generate_final_ocds_json_and_csv

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

parser = argparse.ArgumentParser(description='Generador de csvs, json y poblacion de base de datos '
                                             'ElasticSearch de Open Data')
parser.add_argument('--delete-indexes', help='Borra los indices OCDS de elasticsearch antes de empezar',
                    action='store_true')
parser.add_argument('--only-ocds', help='Solo realiza el ETL para ocds', action='store_true')
parser.add_argument('--only-no-ocds', help='Solo realiza el ETL para no ocds', action='store_true')
parser.add_argument('--only-transform', help='Solo se realiza el paso de transformacion', action='store_true')
parser.add_argument('--only-extract', help='Solo se realiza el paso de extracción', action='store_true')
parser.add_argument('--only-load', help='Solo se realiza el paso de carga', action='store_true')
parser.add_argument('-datasets', help='Solo se realiza el proceso para los datasets indicados', action='store_true')
parser.add_argument('-d', '--datasets', nargs='+', help='Indica la lista de datasets a ejecutar', required=False)
parser.add_argument('--only-final-csv', help='Solo genera los csv finales', action='store_true')
args = parser.parse_args()


def extract_no_ocds(dataset_set):
    path = ''
    for dataset in dataset_set[DATASETS_KEY]:
        # si se pasa el parametro de datasets entonces solo se ejecutan los queries de los datasets indicados
        if (args.datasets and dataset['name'] in args.datasets) or not args.datasets:
            # Algunos datasets no OCDS pueden de todas formas querer ser partidos por anho por ser muy grandes
            # por ejemplo el de precios de items por anho
            if 'by_year' in dataset_set:
                for year in range(INITIAL_YEAR, get_year_until()):
                    path = generate_csv(dataset['query_path'], folder=dataset_set['table'], name=dataset['name'],
                                        params=[year], year=year)
            # el resto de los datasets se ejecutan desde una fecha lejana para traer todos
            else:
                path = generate_csv(dataset['query_path'], folder=dataset_set['table'], name=dataset['name'],
                                    params=[format_date_to_query('1800-01-01')])
    return path


def transform_no_ocds(dataset_set, path):
    json_name = '{}.json'.format(dataset_set['table'])
    convert_to_json(path, json_name, dataset_set['table'], False)


def procesar_dataset_no_ocds(conjuntos_datos):
    logging.info("Procesando conjuntos de datos NO OCDS")
    extract_processes = []
    # extraer en pareleo
    for dataset_set in conjuntos_datos[NO_OCDS_DATASET]:
        p = multiprocessing.Process(target=extract_no_ocds, args=(dataset_set,))
        extract_processes.append(p)
        p.start()
    wait_process(extract_processes)

    # transformar, no en paralelo
    for dataset_set in conjuntos_datos[NO_OCDS_DATASET]:
        path = get_directory(folder=dataset_set['table'])
        if 'by_year' in dataset_set:
            for year in range(INITIAL_YEAR, get_year_until()):
                path = get_directory(folder=dataset_set['table'], anio=year)
                transform_no_ocds(dataset_set, path)
        else:
            transform_no_ocds(dataset_set, path)

    # cargar
    for dataset_set in conjuntos_datos[NO_OCDS_DATASET]:
        path = get_directory(folder=dataset_set['table'])
        json_name = '{}.json'.format(dataset_set['table'])
        if 'by_year' in dataset_set:
            for year in range(INITIAL_YEAR, get_year_until()):
                path = get_directory(folder=dataset_set['table'], anio=year)
                load(os.path.join(path, json_name), dataset_set)
        else:
            load(os.path.join(path, json_name), dataset_set)


def extract_all(conjuntos_datos):
    """
    Ejecuta todos los queries OCDS en paralelo por año y mes desde el año inicial al actual y los guarda en archivos
    CSVs
    :param conjuntos_datos:
    :return:
    """
    extract_processes = []
    for year in range(INITIAL_YEAR, get_year_until()):
        for mes in range(1, 2):
            fecha_desde, fecha_hasta = get_initial_end_date(year, mes)
            path = '{}{}'.format(year, mes)
            p = multiprocessing.Process(target=extract_ocds, args=(conjuntos_datos, [fecha_desde, fecha_hasta], path,
                                                                   args))
            extract_processes.append(p)
            p.start()
    wait_process(extract_processes)


def transform_all():
    for year in range(INITIAL_YEAR, get_year_until()):
        for mes in range(1, 2):
            path = '{}{}'.format(year, mes)
            # En un proceso separado pero de a uno, para liberar la memoria usada en la conversion
            from convert_base import transform_ocds
            p = multiprocessing.Process(target=transform_ocds, args=(path,))
            p.start()
            p.join()
            gc.collect()


def load_all():
    for year in range(INITIAL_YEAR, get_year_until()):
        for mes in range(1, 2):
            path = '{}{}'.format(year, mes)
            p = multiprocessing.Process(target=load_releases_ocds, args=(path,))
            p.start()
            p.join()


def procesar_dataset_ocds(conjuntos_datos):
    if args.only_extract:
        extract_all(conjuntos_datos)
    elif args.only_transform:
        transform_all()
    elif args.only_load:
        load_all()
        insert_records_into_elastic(all_ocids)
        generate_final_ocds_json_and_csv()
    elif args.only_final_csv:
        generate_final_ocds_json_and_csv()

    else:
        extract_all(conjuntos_datos)
        transform_all()
        load_all()
        insert_records_into_elastic(all_ocids)
        generate_final_ocds_json_and_csv()


def delete_indexes():
    delete_index(RELEASES)
    delete_index(RECORDS)


def main():
    datasets = get_dataset_to_process()

    if args.delete_indexes:
        delete_indexes()
    initial_config(datasets)
    if args.only_no_ocds:
        procesar_dataset_no_ocds(datasets)
    elif args.only_ocds:

        procesar_dataset_ocds(datasets)
    else:
        procesar_dataset_no_ocds(datasets)
        procesar_dataset_ocds(datasets)


main()
