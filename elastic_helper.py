import datetime
import logging
from copy import copy

from elasticsearch import Elasticsearch, helpers
from libcoveocds.config import LibCoveOCDSConfig
from libcoveocds.schema import SchemaOCDS
from ocdsmerge import merge

import config
from const import RELEASES, RELEASES_BASE_URL, EXTENDED_SCHEMA_FILE, OCDS_VERSION, PACKAGE_DATA, RECORDS, \
    MAX_DOCUMENTS_AT_TIME, NO_OCDS_DATASET
from helper import load_json, get_initial_end_date

es = Elasticsearch(config.elastic, timeout=1000)
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)


def get_elastic_bulk_row(collection_type, collection_table, item):
    if collection_table == RELEASES:
        item['tag'] = get_release_tag(item)
    if collection_table != RELEASES and collection_table != RECORDS:
        item['date'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S%z')
    bulk = {
        '_index': collection_table,
        '_type': collection_type,
        '_id': item['id'] if collection_table != RECORDS else item['ocid'],
        '_source': item,
    }
    return bulk


def delete_index(index):
    es.indices.delete(index=index, ignore=[400, 404])


def get_record(releases):
    record = {'ocid': releases[0]['ocid'], RELEASES: [],
              'compiledRelease': merge(releases, schema=EXTENDED_SCHEMA_FILE)}
    for release in releases:
        record[RELEASES].append({
            'date': release['date'],
            'tag': release['tag'],
            'url': RELEASES_BASE_URL + release['id']
        })
    record['compiledRelease']['tag'] = ['compiled']
    return record


def get_last_index_date(index=RELEASES):
    try:
        max_date = es.search(index=index, body={
            'aggs': {
                'max_date': {'max': {'field': 'date', 'format': 'yyyy-MM-dd'}}
            }
        }
                             )
        return datetime.datetime.strptime(max_date['aggregations']['max_date']['value_as_string'],
                                      '%Y-%m-%d')
    except Exception:
        return '1800-01-01'


def find_by_year(year, index='records', mes=1):
    records = copy(PACKAGE_DATA)
    records[index] = []
    to_search = 'compiledRelease.date' if index == 'records' else 'date'
    fecha_desde, fecha_hasta = get_initial_end_date(year, mes)
    data = es.search(index=index, scroll='2m', size='500', body={
        'query': {
            'range': {
                to_search: {
                    'gte': fecha_desde.replace("'", ""),
                    'lte': fecha_hasta.replace("'", "")
                }
            }
        }
    })
    while data['hits']['hits']:
        for record in data['hits']['hits']:
            records[index].append(record['_source'])
        data = es.scroll(scroll_id=data['_scroll_id'], scroll='2m')
    return records


def find_releases(ocid):
    logging.info(ocid)
    res = es.search(index='releases', body={
        'query': {
            'match': {'ocid': ocid}
        },
        'sort': {
            'date': {'order': 'desc'}
        }
    }
                    )
    releases = []
    for release in res['hits']['hits']:
        releases.append(release['_source'])
    return releases


def initial_config(datasets):
    """
    This method creates the releases index if doesnt exist with an extended total field number due the high number
    of ocds fields to insert. Also, this method takes all the extensions declarated in the PACKAGE_DATA variable and
    generates the extended schema to use in the json generation.
    :return:
    """
    logging.info('Paso 1: Configuración inicial')

    for dataset_set in datasets[NO_OCDS_DATASET]:
        if not es.indices.exists(dataset_set['table']):
            request_body = {
                    'max_result_window': 60000
            }
            es.indices.create(index=dataset_set['table'], body=request_body)
    for ocds in [RELEASES, RECORDS]:
        if not es.indices.exists(ocds):
            logging.info('Indice de releases no existente, creando')
            request_body = {
                'settings': {
                    'index': {
                        'mapping': {
                            'total_fields': {
                                'limit': '5000'
                            }
                        }
                    }
                },
                'max_result_window': 60000
            }
            es.indices.create(index=ocds, body=request_body)

    lib_cove_ocds_config = LibCoveOCDSConfig()
    schema_ocds = SchemaOCDS(OCDS_VERSION, PACKAGE_DATA, lib_cove_ocds_config=lib_cove_ocds_config)
    schema_ocds.create_extended_release_schema_file('', '')


def insert_records_into_elastic(inserted_ocids):
    """
    Given a list of inserted ocids, this method search their releases, generates the record for each ocid and
    insert it to the elastic search.
    :param inserted_ocids:
    :return:
    """
    logging.info('Insertando records')
    es.indices.refresh(RELEASES)
    collection = {'table': RECORDS, 'type': 'record', 'id': 'ocid'}
    records = []
    count = 0
    for ocid in inserted_ocids:
        releases = find_releases(ocid)
        record = get_record(releases)
        records.append(get_elastic_bulk_row(collection['type'], collection['table'], record))
        count = count + 1
        if count == MAX_DOCUMENTS_AT_TIME:
            helpers.bulk(es, records, refresh='wait_for')
            records = []
            count = 0
    if MAX_DOCUMENTS_AT_TIME > count > 0:
        helpers.bulk(es, records, refresh='wait_for')
    es.indices.refresh(RECORDS)


def insert_into_elastic_search(filename, collection, type='json'):
    logging.info('Insertando %s en elasticsearch' % filename)
    inserted_ocids = []
    releases = []
    data = load_json(filename, collection, type)
    for items in data:
        for item in items:
            bulk_item = get_elastic_bulk_row(collection['type'], collection['table'], item)
            releases.append(bulk_item)
            if len(releases) % MAX_DOCUMENTS_AT_TIME == 0:
                helpers.bulk(es, releases)
                releases = []
            if 'ocid' in item:
                inserted_ocids.append(item['ocid'].split('ocds-03ad3f-')[1])

    if len(releases) % MAX_DOCUMENTS_AT_TIME != 0:
        helpers.bulk(es, releases)
    return inserted_ocids


def get_release_tag(release):
    tag = []
    if 'planning' in release:
        tag.append('planning')
    if 'tender' in release or 'preQualification' in release:
        tag.append('tender')
    if 'awards' in release:
        tag.append('award')
    if 'contracts' in release:
        tag.append('contract')
        for contract in release['contracts']:
            if 'status' in contract and contract['status'] == 'terminated' and 'contractTermination' not in tag:
                tag.append('contractTermination')
            if 'implementation' in contract and 'implementation' not in tag:
                tag.append('implementation')
            if 'amendments' in contract and 'contractAmendment' not in tag:
                tag.append('contractAmendment')
                tag.remove('contract')
                if 'planning' in tag:
                    tag.remove('planning')
                    tag.append('planningUpdate')
    return tag
