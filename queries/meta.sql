SELECT
  DISTINCT
  'es'                                                                    AS language,
  'ocds-03ad3f-' || cast(planificacion.nro_planificacion AS VARCHAR)                          AS ocid,
  coalesce(convocatoria.slug, planificacion.nro_planificacion :: TEXT) || '-' ||
  extract(EPOCH FROM tag.ultima_fecha_publicacion) :: TEXT                AS id,
  'tender'                                                                AS "initiationType",

  'x_DNCP-SICP-CODE-' || convocantes_entidad.codigo_sicp                  AS "buyer/id",
  convocantes_entidad.nombre                                              AS "buyer/name"
FROM planificacion
  JOIN vw_busqueda_convocatorias_fecha AS tag ON tag.nro_planificacion = planificacion.nro_planificacion
  JOIN vw_convocantes AS convocantes_entidad ON convocantes_entidad.codigo_sicp = planificacion.codigo_sicp
  LEFT JOIN convocatoria ON convocatoria.planificacion_id = planificacion.id

WHERE tag.ultima_fecha_publicacion BETWEEN {0} AND {1}
