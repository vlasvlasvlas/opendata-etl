SELECT DISTINCT
  'ocds-03ad3f-' || cast(planificacion.nro_planificacion AS VARCHAR)           AS "ocid",
  convocatoria.slug || '-' ||
  extract(EPOCH FROM tag.ultima_fecha_publicacion) :: TEXT AS "id",
  convocatoria.slug                                             AS "tender/id",
  convocatoria.descripcion                                      AS "tender/title",
  CASE WHEN
    convocatoria.estado = 'PUB'
    OR convocatoria.estado = 'IMP_PAR'
    OR convocatoria.estado = 'CIMP'
    THEN
      'active'
  WHEN convocatoria.estado = 'CAN'
    THEN
      'cancelled'
  WHEN convocatoria.estado = 'DES'
       OR convocatoria.estado = 'SUS'
       OR convocatoria.estado = 'CAD'
       OR convocatoria.estado = 'IMP_SUS'
       OR convocatoria.estado = 'IMP_PAR_SUS'
    THEN
      'unsuccessful'
  WHEN convocatoria.estado = 'ADJ'
    THEN 'complete'
  WHEN convocatoria.estado = 'ANU'
    THEN 'withdrawn'
  END                                                      AS "tender/status",
  convocatoria.lugar_consulta                                   AS "tender/enquiriesAddress/streetAddress",
  convocatoria.monto_total                                      AS "tender/value/amount"
FROM convocatoria
  JOIN planificacion ON planificacion.id = convocatoria.planificacion_id
  JOIN vw_busqueda_convocatorias_fecha AS tag ON tag.nro_planificacion = planificacion.nro_planificacion
WHERE tag.ultima_fecha_publicacion BETWEEN {0} AND {1}
