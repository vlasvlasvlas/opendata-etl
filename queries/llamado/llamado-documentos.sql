SELECT DISTINCT
  'ocds-03ad3f-' || cast(planificacion.nro_planificacion AS VARCHAR)                       AS ocid,
  convocatoria.slug || '-' ||
  extract(EPOCH FROM tag.ultima_fecha_publicacion) :: TEXT             AS id,
  convocatoria.slug                                                         AS "tender/id",
  convocatoria_adjunto.id                                                   AS "tender/documents/0/id",
  TO_CHAR(convocatoria_adjunto.fecha_publicacion, 'yyyy-mm-ddThh24:MI:SSZ') AS "tender/documents/0/datePublished",
  convocatoria_adjunto.nombre                                               AS "tender/documents/0/title",
  'https://www.contrataciones.gov.py/documentos/download/convocatoria/' ||
  convocatoria_adjunto.id                                                   AS "tender/documents/0/url",
  'es'                                                                 AS "tender/documents/0/language",
  tipo_documento.descripcion                                           AS "tender/documents/0/documentType"

FROM convocatoria_adjunto
  JOIN convocatoria ON convocatoria_adjunto.convocatoria_id = convocatoria.id
  JOIN tipo_documento ON convocatoria_adjunto.tipo_documento_id = tipo_documento.id
  JOIN planificacion ON planificacion.id = convocatoria.planificacion_id
  JOIN vw_busqueda_convocatorias_fecha AS tag ON tag.nro_planificacion = planificacion.nro_planificacion
WHERE tag.ultima_fecha_publicacion BETWEEN {0} AND {1}