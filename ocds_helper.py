import gc
import json
import multiprocessing
import os

from libcoveocds.config import LibCoveOCDSConfig
from libcove.lib.converters import convert_json

from const import EXTENDED_SCHEMA_FILE, INITIAL_YEAR, OCDS_DATASET, RECORDS, RELEASES
from elastic_helper import find_by_year
from helper import get_year_until, get_directory, generate_csvs


def convert_to_csv(path, name):
    lib_cove_ocds_config = LibCoveOCDSConfig()
    lib_cove_ocds_config.config['convert_titles'] = True
    lib_cove_ocds_config.config['flatten_tool'] = {'disable_local_refs': True, 'remove_empty_schema_columns': True}
    convert_json(path, '', name, lib_cove_ocds_config, schema_url=EXTENDED_SCHEMA_FILE, flatten=True,
                 cache=False)


def generate(year, index_type, folder=OCDS_DATASET):
    for mes in range(1, 2):
        json_name = '{}-{}'.format(year, mes)
        directory = get_directory(folder=folder, anio=json_name)
        records = find_by_year(year, index_type, mes=mes)
        name = os.path.join(directory, '{}-{}.json'.format(index_type, json_name))
        with open(name, 'w') as file:
            json.dump(records, file, indent=4, ensure_ascii=False)
        #convert_to_csv(directory, name)
    #generate_csvs()


def generate_final_ocds_json_and_csv():
    """
    Search in the elastic all the records and releases per year, save it them as json and then transform the json file into csv
    and excel file.
    :return:
    """
    for index_type in [RELEASES, RECORDS]:
        for year in range(INITIAL_YEAR, get_year_until()):
            p = multiprocessing.Process(target=generate, args=(year, index_type))
            p.start()
            p.join()
            gc.collect()


