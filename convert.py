# -*- coding: utf-8 -*-
"""Importador de datos de la base de datos de la DNCP a formatos abiertos en Elasticsearch

Esta clase tiene como función tomar querys sobre las tablas relacionales de la base de datos de la
DNCP, ejecutarlos, transformarlos y guardarlos finalmente como JSON en Elasticsearch.

Actualmente soporta dos tipos de queries:

- ocds:
    Los queries OCDS se refieren a aquellos conjuntos de datos que requieran ser publicados en
    formato Open Contracting Data Standard. Para poder hacer esta transformación se toman todos los queries
    listados bajo ocds, se ejecutan, se guardan como CSV, se convierten a JSON con la herramienta flattening tool
    OCDS y se envian estos jsons al índice 'releases' del Elasticsearch

- No ocds:
    Para los otros conjuntos de datos de la DNCP que no van dentro del estándar OCDS se procede solamente a
    ejecutar los queries, guardarlos como CSV y enviarlos directamente al Elasticsearch

"""
import argparse
import datetime
import logging
import multiprocessing
import os

from const import NO_OCDS_DATASET, DATASETS_KEY, OCDS_DATASET
from convert_base import extract_ocds, transform_ocds, load_releases_ocds, all_ocids, generate_csv, load, \
    extract_ocds_for_dataset
from elastic_helper import insert_records_into_elastic, \
    get_last_index_date, initial_config
from helper import get_directory, convert_to_json, get_dataset_to_process, format_date_to_query, wait_process
from ocds_helper import generate_final_ocds_json_and_csv

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

parser = argparse.ArgumentParser(description='Generador de csvs, json y poblacion de base de datos '
                                             'ElasticSearch de Open Data')
parser.add_argument('--only-final-csv', help='Solo genera los csv finales', action='store_true')
parser.add_argument('-d', '--datasets', nargs='+', help='Indica la lista de datasets a ejecutar', required=False)
parser.add_argument('--only-ocds', help='Solo realiza el ETL para ocds', action='store_true')
parser.add_argument('--only-no-ocds', help='Solo realiza el ETL para no ocds', action='store_true')

args = parser.parse_args()


def extract_no_ocds(dataset_set):
    last_run = get_last_index_date(index=dataset_set['table'])
    fecha_a_buscar = format_date_to_query(last_run)
    for dataset in dataset_set[DATASETS_KEY]:
        if 'by_year' in dataset_set:
            fecha_a_buscar = datetime.datetime.now().year
        generate_csv(dataset['query_path'], folder=dataset_set['table'], name=dataset['name'], params=[fecha_a_buscar])


def transform_no_ocds(dataset_set, path):
    json_name = '{}.json'.format(dataset_set['table'])
    convert_to_json(path, json_name, dataset_set['table'], False)
    return os.path.join(path, json_name)


def procesar_dataset_no_ocds(conjuntos_datos):
    logging.info("Procesando conjuntos de datos NO OCDS")
    extract_processes = []
    for dataset_set in conjuntos_datos[NO_OCDS_DATASET]:
        p = multiprocessing.Process(target=extract_no_ocds, args=(dataset_set,))
        extract_processes.append(p)
        p.start()
    wait_process(extract_processes)
    for dataset_set in conjuntos_datos[NO_OCDS_DATASET]:
        path = get_directory(folder=dataset_set['table'])
        json_file = transform_no_ocds(dataset_set, path)
        load(json_file, dataset_set)


def procesar_dataset_ocds(conjuntos_datos):
    last_release_date = get_last_index_date()
    #last_release_date = '2019-08-14'
    datos_ocds = conjuntos_datos[OCDS_DATASET]
    extract_processes = []
    for dataset in datos_ocds:
        p = multiprocessing.Process(target=extract_ocds_for_dataset,
                                    args=(dataset, [format_date_to_query(last_release_date),
                                                    format_date_to_query(datetime.datetime.now())], last_release_date,
                                          args))
        p.start()
        extract_processes.append(p)
    wait_process(extract_processes)
    transform_ocds(last_release_date)
    load_releases_ocds(last_release_date)
    insert_records_into_elastic(all_ocids)


def main():
    initial_config()
    datasets = get_dataset_to_process()
    if args.only_final_csv:
        generate_final_ocds_json_and_csv()
    else:
        if args.only_no_ocds:
            procesar_dataset_no_ocds(datasets)
        elif args.only_ocds:
            procesar_dataset_ocds(datasets)
        else:
            procesar_dataset_no_ocds(datasets)
            procesar_dataset_ocds(datasets)


main()
